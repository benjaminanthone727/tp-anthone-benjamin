<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_conteneur extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
    }

    public function select_all()
    {
        $query = $this->db->select('Id, LatLng, VolumeMesureActuel,AddrEmplacement')
            ->from('conteneur')
            ->get();
        return $query->result_array();
    }


    public function select_detail_by_conteneur($prmid)
    {
        $query = $this->db->select('Id, LatLng, TourneeStandardId, AddrEmplacement, VolumeMesureActuel')
            ->from('conteneur')
            ->where('TourneeStandardId', $prmid)
            ->get();
        return $query->result_array();
    }

    public function select_search_by_page($search)
    {
        $config_p['reuse_query_string'] = true;
        $query = $this->db->select('Id, AddrEmplacement')
            ->from('conteneur')
            ->like('AddrEmplacement', $search)
            ->get();
        return $query->result_array();
    }





    public function select_optimisee()
    {
        $query = $this->db->select('Id, LatLng, VolumeMesureActuel, AddrEmplacement')
            ->from('conteneur')
            //->where('VolumeMesureActuel',"3")
            ->get();
        return $query->result_array();
    }

    
    public function select_optimisee_detail($prmid)
    {
        $query = $this->db->select('Id, LatLng, VolumeMesureActuel, AddrEmplacement, TourneeStandardId')
            ->from('conteneur')
            ->where('TourneeStandardId', $prmid)
            ->get();
        return $query->result_array();
    }


}

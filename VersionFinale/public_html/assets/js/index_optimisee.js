//Fichier JavaScript permettant la création de la carte Leaflet ainsi que ses pointeurs.
//Créer par Gabriel Vanloot 


var tableauTournée = []; //Création tableau

var marker = null;

var carte = null;

var mapMarkers = []; // Création d'un tableau 
var latlngarray = [];



var remplissage = 0;
var totalRemplissage = 0;
var id = 0;

var gps = null;



$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "http://localhost:82/CollecteVerre/index.php/REST/conteneur/index_optimisee/" + idTournee, //adresse Url du service Web avec le nom de la fonction 
        dataType: "json",
        json: "json",
        success: onGetSuccess,
        error: onGetError
    });

    function onGetSuccess(reponse, status) {

        //--------------------------------Création de la carte Leaflet-----------------------------//
        var carte_optimisee = L.map('macarte_optimisee').setView([50.6333, 3.0667], 9);
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(carte_optimisee);
        //------------------------------Fin Création de la carte Leaflet---------------------------//
        for (var i = 0; i <= 7; i++) {

            // Image du marker 
            var greenIcon = new L.icon({ iconUrl: 'http://localhost:82/CollecteVerre/assets/js/img/marker-icon-green.png' })

            var point = reponse[i].LatLng; // Recupere la longitude et latitude 
            var Lng = point.split(","); // Création d'un tableau avec lat index 0 et lng index 1

            id = reponse[i].Id; // Recupére l'id 
            var pointLat = Lng[0]; // Stocke l'atitude 
            pointLat = parseFloat(pointLat); // Convertie en nombre flottant 
            var pointLng = Lng[1]; // Stocke la longitude
            pointLng = parseFloat(pointLng); // Convertie en nombre flottant

            var ltln = new L.LatLng(pointLat, pointLng); // Instancie un nouveau objet 


            latlngarray.push(ltln);
            //: Rajoute le poit dans un tableau 

            marker = L.marker([pointLat, pointLng], { icon: greenIcon }); // Stocke les coordonnées du marker 

            mapMarkers.push(marker); //Ajoute les markers dans le tableau 
        }
        // Enregistre le waypoint pour le gps 
        gps = L.Routing.control({
            waypoints: latlngarray
        });
        gps.addTo(carte_optimisee); // Affiche sur la carte 
        /*
                for (var i = 0; i <= 100; i++) {
                    var newLength = tableauTournée.push(reponse[i]);

                    var point = tableauTournée[i].LatLng; //enregistre les coordonnes dans la variable point
                    var Lng = point.split(","); //sépare lat et long en créant un tableau
                    var pointLat = Lng[0]; //enregistre la latitude dans la premiere case d'un tableau
                    var pointLng = Lng[1]; //enregistre la latitude dans la deuxieme case d'un tableau

                    var greenIcon = new L.Icon({ //Pour un icon vert
                        iconUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/conteneur-verre.pngg',
                        shadowUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-shadow.png',
                        iconSize: [55, 50],
                        iconAnchor: [12, 41],
                        popupAnchor: [1, -34],
                        shadowSize: [41, 41]
                    });

                    var goldIcon = new L.Icon({ //Création icon gold
                        iconUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/conteneur-jaune.png',
                        shadowUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-shadow.png',
                        iconSize: [35, 40],
                        iconAnchor: [12, 41],
                        popupAnchor: [1, -34],
                        shadowSize: [41, 41]
                    });

                    var orangeIcon = new L.Icon({ //Création icon orange
                        iconUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/conteneur-orange.png',
                        shadowUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-shadow.png',
                        iconSize: [35, 40],
                        iconAnchor: [12, 41],
                        popupAnchor: [1, -34],
                        shadowSize: [41, 41]
                    });

                    var redIcon = new L.Icon({ //Création icon rouge
                        iconUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/conteneur-rouge.png',
                        shadowUrl: 'http://localhost:82/CollecteVerre/assets/image/imgSite/marker-shadow.png',
                        iconSize: [35, 40],
                        iconAnchor: [12, 41],
                        popupAnchor: [1, -34],
                        shadowSize: [41, 41]
                    });

                    var blackIcon = new L.Icon({ //Création icon noir
                        iconUrl: 'http://localhost/CollecteVerre/assets/image/imgSite/conteneur-noir.png',
                        shadowUrl: 'http://localhost/CollecteVerre/assets/image/imgSite/marker-shadow.png',
                        iconSize: [35, 40],
                        iconAnchor: [12, 41],
                        popupAnchor: [1, -34],
                        shadowSize: [41, 41]
                    });
                    var tauxremplissage;
                    //Ici trouver un moyen de recupéré la tournée standard????????
                    if (tableauTournée[i].TourneeStandardId == idTournee) { //Place les points de la tournée standard
                        switch (tableauTournée[i].VolumeMesureActuel != null) { //Si le taux de remplissage est différent de null

                            case tableauTournée[i].VolumeMesureActuel == 0: //si le taux de remplissage vaut 0
                                tauxremplissage = "0%"; //enregistre "0%" dans un variable local 
                                var marker = L.marker([pointLat, pointLng], { icon: greenIcon }); //Place le marker vert sur la carte
                                marker.addTo(carte_optimisee);
                                break;

                            case tableauTournée[i].VolumeMesureActuel == 1:
                                tauxremplissage = "25%";
                                var marker = L.marker([pointLat, pointLng], { icon: goldIcon });
                                marker.addTo(carte_optimisee);
                                break;

                            case tableauTournée[i].VolumeMesureActuel == 2:
                                tauxremplissage = "50%";
                                var marker = L.marker([pointLat, pointLng], { icon: orangeIcon });
                                marker.addTo(carte_optimisee);
                                break;

                            case tableauTournée[i].VolumeMesureActuel == 3:
                                tauxremplissage = "75%";
                                var marker = L.marker([pointLat, pointLng], { icon: redIcon });
                                marker.addTo(carte_optimisee);
                                break;

                            case tableauTournée[i].VolumeMesureActuel == 4:
                                tauxremplissage = "100%";
                                var marker = L.marker([pointLat, pointLng], { icon: blackIcon });
                                marker.addTo(carte_optimisee);
                                break;

                            default:
                                tauxremplissage = "404";
                                alert("erreur bdd")
                        }
                    }
                       
                    if (tableauTournée[i].VolumeMesureActuel >= 3) { //Si les conteneurs sont a plus de 75%
                        switch (tableauTournée[i].VolumeMesureActuel != null) { //Si le taux de remplissage est différent de null
                            case tableauTournée[i].VolumeMesureActuel == 3:
                                tauxremplissage = "75%";
                                var marker = L.marker([pointLat, pointLng], { icon: redIcon });
                                marker.addTo(carte_optimisee);
                                break;

                            case tableauTournée[i].VolumeMesureActuel == 4:
                                tauxremplissage = "100%";
                                var marker = L.marker([pointLat, pointLng], { icon: blackIcon });
                                marker.addTo(carte_optimisee);
                                break;

                            default:
                                tauxremplissage = "404";
                                alert("erreur bdd")
                        }
                    }

                    marker.bindPopup(''); //Initialise la bulle de texte
                    var mapopup = marker.getPopup();

                    mapopup.setContent('Remplissage = ' + tauxremplissage + '  ' + reponse[i].AddrEmplacement); //personnalise le contenu de la bulle de texte
                    marker.openPopup();
                }
                */
    }



    //--------------------------------------------Si la connexion à echoué----------------------------//
    function onGetError(reponse, status) {
        alert("erreur 404");
    }
});
//Création des variables 
var marker = null;
var carte = null;
var mapMarkers = [];
var latlngarray = [];
var mapopup
var remplissage = 0;
var totalRemplissage = 0;
var id = 0;
var gps = null;

var elem = document.getElementById("myBar"); // recupére l'id de la barre de progression. 

$(document).ready(function() {

    /*********************************** Création de la map  ****************************************************************/
    carte = L.map('macarte').setView([50.6333, 3.0667], 9);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(carte);
    /*--------------------TEST GPS -------------------------------------------------------
        L.Routing.control({
            waypoints: [
                L.latLng(57.74, 11.94),
                L.latLng(57.6792, 11.949)
            ]
        }).addTo(carte);
    ------------------------------------------------------------------------------------------*/

    elem.style.width = totalRemplissage + "%"; // On affiche la bar de progression 

    document.getElementById("optimisation1").style.display = "none"; //le bouton n'est pas afficher
    document.getElementById("optimisation2").style.display = "none"; //le bouton n'est pas afficher
    document.getElementById("optimisation3").style.display = "none"; //le bouton n'est pas afficher
    document.getElementById("optimisation4").style.display = "none"; //le bouton n'est pas afficher

    /********************** Quand on click sur le bouton la fonction TourneeNieppe est executé  ***************************************/
    $(".btn").click(Tournee);

    function Tournee() {
        var idbtn = $(this).data("id"); // On stocke le data-id du bouton dans une variable 
        // Si le data-id du bouton est égale à 7 alors on affiche tout les conteneurs 
        if (idbtn === 7) {
            TotalConteneur(); // Appelle d'une fonction qui permet d'afficher tous les conteneurs sur la map. 
            // Sinon on exécute le code qui permet d'afficher les conteneurs par rapport à la tournées et trace le gps.    
        } else {
            //--------------------------------------------------------------------Codage_Gabriel-------------------------------------------------------------//
            $("#optimisation1").attr("href", "http://localhost:82/CollecteVerre/index.php/C_tournee_optimisee/index/" + idbtn.toString()); //recupère le numero de la tournée et l'envoie
            //------------------------------------------------------------------Fin_Codage_Gabriel-----------------------------------------------------------//

            //Si le tableau contient des choses alors
            if (mapMarkers.length > 0) {
                //Il va parcourir le tableau 
                for (var i = 0; i < mapMarkers.length; i++) {
                    totalRemplissage = 0; // On rénitialise la variable à 0. 
                    carte.removeLayer(mapMarkers[i]); // Supprimer les markers du tableau. 
                    elem.style.width = totalRemplissage + "%"; // réinitialise la barre de progression. 
                }
                //Si le gps est différent de null c'est à dire qu-il ya des waypoints alors 
            }
            if (gps !== null) {
                gps.getPlan().setWaypoints({ latLng: L.latLng([0, 0]) }); // On remet la longitude et la latitude à 0.
                gps = L.Routing.control({
                    waypoints: [] // On indique qu'il n'ya plus de waypoint pour tracer le GPS.
                });
                latlngarray = []; // Indique le tableau est vide. 
                $(".leaflet-routing-container").remove(); // Supprime la route. 
            }


            /**********************************  Requête Ajax *************************************************************************/
            $.ajax({
                type: "GET",
                url: "http://localhost:82/CollecteVerre/index.php/REST/conteneur/index/" + idbtn, // Url du service web + le paramétre data-id  du bouton 
                dataType: "json",
                json: "json",
                success: onGetSuccess,
                error: onGetError
            });
            /**********************************  Si la connexion est établie  *************************************************************************/
            function onGetSuccess(reponse, status) {

                Bouton(); //Fonction qui permet de cachers les boutons optimisée lors d'un clique sur une tournée.

                // Place seulement 8 marker sur la carte 
                for (var i = 0; i <= 7; i++) {


                    var greenIcon = new L.icon({ iconUrl: 'http://localhost:82/CollecteVerre/assets/js/img/marker-icon-green.png' })

                    var point = reponse[i].LatLng; // Recupere la longitude et latitude 
                    var Lng = point.split(","); // Création d'un tableau avec lat index 0 et lng index 1

                    id = reponse[i].Id; // Stock l'id dans une variable.
                    var pointLat = Lng[0]; // Stocke l'altitude.
                    var pointLng = Lng[1]; // Stocke la longitude.

                    var ltln = new L.LatLng(pointLat, pointLng); // Instancie un nouvelle objet avec les coordonées qui ont etaient convertits en float. 


                    latlngarray.push(ltln); // Ajoute dans le tableau la longitude et latitude. 

                    remplissage = VolumeMesure(i); // On stock la valeur retournée par la fonction 
                    totalRemplissage = remplissage + totalRemplissage; // On addtionne l'ancienne avec la nouvelle 
                    elem.style.width = totalRemplissage + "%"; // On affiche la bar de progression 
                    elem.innerHTML = totalRemplissage + "%";

                    marker = L.marker({ icon: greenIcon }); // Stocke les coordonnées du marker avec l'image  

                    mapMarkers.push(marker); //Ajoute les markers dans le tableau 
                }
                /*----------------------------------- FIN DU Pour ----------------------------*/
                // Traçage du GPS 
                gps = L.Routing.control({
                    waypoints: latlngarray, // Permet de tracer le gps avec tout les Markers grâce au tableau "latlngarray"
                });
                gps.addTo(carte); // Affiche sur la carte 

                //Permet d'afficher sur le numero de la tournée. 
                document.getElementById("NumTournees").innerHTML = "Visualiser les niveaux de vos conteneurs de la tournée " + "<br>" + idbtn;


                function Bouton() {
                    if (idbtn === 3) {
                        document.getElementById("optimisation1").style.display = "block" //le bouton s'affiche
                        $("#optimisation1").attr("href", "http://localhost:82/CollecteVerre/index.php/C_tournee_optimisee/index/" + idbtn.toString()); //recupère le numero de la tournée et l'envoie

                        document.getElementById("optimisation2").style.display = "none"; //le bouton n'est pas afficher
                        document.getElementById("optimisation3").style.display = "none"; //le bouton n'est pas afficher
                        document.getElementById("optimisation4").style.display = "none"; //le bouton n'est pas afficher

                    }
                    if (idbtn === 4) {
                        document.getElementById("optimisation2").style.display = "block" //le bouton s'affiche
                        $("#optimisation2").attr("href", "http://localhost:82/CollecteVerre/index.php/C_tournee_optimisee/index/" + idbtn.toString()); //recupère le numero de la tournée et l'envoie

                        document.getElementById("optimisation3").style.display = "none"; //le bouton n'est pas afficher
                        document.getElementById("optimisation4").style.display = "none"; //le bouton n'est pas afficher
                        document.getElementById("optimisation1").style.display = "none"; //le bouton n'est pas afficher
                    }
                    if (idbtn === 5) {
                        document.getElementById("optimisation3").style.display = "block" //le bouton s'affiche
                        $("#optimisation3").attr("href", "http://localhost:82/CollecteVerre/index.php/C_tournee_optimisee/index/" + idbtn.toString()); //recupère le numero de la tournée et l'envoie

                        document.getElementById("optimisation1").style.display = "none"; //le bouton n'est pas afficher
                        document.getElementById("optimisation2").style.display = "none"; //le bouton n'est pas afficher
                        document.getElementById("optimisation4").style.display = "none"; //le bouton n'est pas afficher
                    }
                    if (idbtn === 6) {
                        document.getElementById("optimisation4").style.display = "block" //le bouton s'affiche
                        $("#optimisation4").attr("href", "http://localhost:82/CollecteVerre/index.php/C_tournee_optimisee/index/" + idbtn.toString()); //recupère le numero de la tournée et l'envoie

                        document.getElementById("optimisation1").style.display = "none"; //le bouton n'est pas afficher
                        document.getElementById("optimisation2").style.display = "none"; //le bouton n'est pas afficher
                        document.getElementById("optimisation3").style.display = "none"; //le bouton n'est pas afficher
                    }



                }
                /***************Fonction qui permet de calculer le ramassage total d'une collecte */
                function VolumeMesure(i) {


                    var MesureActuel = reponse[i].VolumeMesureActuel; // Stock la valeur du volume actuel d'un conteneur 
                    var tauxremplissage;


                    switch (MesureActuel != null) { //Si le taux de remplissage est différent de null

                        case MesureActuel == 0: //si le taux de remplissage vaut 0
                            tauxremplissage = 0; //enregistre "0%" dans une variable local 
                            break;

                        case MesureActuel == 1:
                            tauxremplissage = 25;
                            break;

                        case MesureActuel == 2:
                            tauxremplissage = 50;
                            break;

                        case MesureActuel == 3:
                            tauxremplissage = 75;
                            break;

                        case MesureActuel == 4:
                            tauxremplissage = 100;
                            break;
                        default:
                            tauxremplissage = 404;
                            alert("erreur bdd")
                    };

                    return tauxremplissage; // On return la valeur du taux de remplissage 
                }
            }
        }
        /**********************************  Si la connexion à échoué   *************************************************************************/
        function onGetError(reponse, status) {
            alert("erreur 404");
        }
    }



    function TotalConteneur() {

        if (mapMarkers.length > 0) {
            //Il va parcourir le tableau 
            for (var i = 0; i < mapMarkers.length; i++) {
                totalRemplissage = 0;
                carte.removeLayer(mapMarkers[i]); // Supprimer les markers du tableau 
                elem.style.width = totalRemplissage + "%"; // réinitialise la barre de progression 
            }

        }
        if (gps !== null) {
            gps.getPlan().setWaypoints({ latLng: L.latLng([0, 0]) }); // On remet la longitude et la latitude à 0.
            gps = L.Routing.control({
                waypoints: [] // On indique qu'il n'ya plus de waypoint pour tracer le GPS.
            });
            latlngarray = []; // Indique le tableau est vide. 
            $(".leaflet-routing-container").remove(); // Supprime la route. 
        }

        /**********************************  Requête Ajax *************************************************************************/
        $.ajax({
            type: "GET",
            url: "http://localhost:82/CollecteVerre/index.php/REST/conteneur/index/", // Url du service web + le paramétre data-id  du bouton 
            dataType: "json",
            json: "json",
            success: onGetSuccess,
            error: onGetError
        });
        /**********************************  Si la connexion est établie  *************************************************************************/
        function onGetSuccess(reponse, status) {

            // Place seuelement tout les markers sur la carte
            for (var i = 0; i <= 100; i++) {

                // Image du marker 
                var greenIcon = new L.icon({ iconUrl: 'http://localhost:82/CollecteVerre/assets/js/img/marker-icon-green.png' })

                var point = reponse[i].LatLng; // Recupere la longitude et latitude 
                var Lng = point.split(","); // Création d'un tableau avec lat index 0 et lng index 1

                var pointLat = Lng[0]; // Stocke l'atitude 
                var pointLng = Lng[1]; // Stocke la longitude 

                marker = L.marker([pointLat, pointLng], { icon: greenIcon }); // Stocke les coordonnées du marker 
                marker.addTo(carte); // Place le marker sur la carte 
                mapMarkers.push(marker); //Ajoute les markers dans le tableau

                marker.bindPopup(''); //Initialise la bulle de texte
                mapopup = marker.getPopup();

                var volumeConteneur = VolumeMesure(i);

                mapopup.setContent('Remplissage = ' + volumeConteneur + '%' + '' + reponse[i].AddrEmplacement); //personnalise le contenu de la bulle de texte
                marker.openPopup();
            }
            /***************Fonction qui permet de calculer le ramassage total d'une collecte */
            function VolumeMesure(i) {


                var MesureActuel = reponse[i].VolumeMesureActuel; // Stock la valeur du volume actuel d'un conteneur 
                var tauxremplissage;


                switch (MesureActuel != null) { //Si le taux de remplissage est différent de null

                    case MesureActuel == 0: //si le taux de remplissage vaut 0
                        tauxremplissage = 0; //enregistre "0%" dans une variable local 
                        break;

                    case MesureActuel == 1:
                        tauxremplissage = 25;
                        break;

                    case MesureActuel == 2:
                        tauxremplissage = 50;
                        break;

                    case MesureActuel == 3:
                        tauxremplissage = 75;
                        break;

                    case MesureActuel == 4:
                        tauxremplissage = 100;
                        break;
                    default:
                        tauxremplissage = 404;
                        alert("erreur bdd")
                };

                return tauxremplissage; // On return la valeur du taux de remplissage 
            }
        }
        /**********************************  Si la connexion à échoué   *************************************************************************/
        function onGetError(reponse, status) {
            alert("erreur 404");
        }
    }

});
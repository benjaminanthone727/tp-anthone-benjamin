
<section class="page-section clearfix">
    <div class="container">
        <div class="intro">
            <img class="intro-img img-fluid mb-3 mb-lg-0 rounded" src=<?php echo base_url("assets/image/carte-smictom.png") ?> alt="">
            <div class="intro-text left-0 text-center bg-faded p-5 rounded">
                <h2 class="section-heading mb-4">
                    <span class="section-heading-upper">Objectifs </span>
                    <span class="section-heading-lower">De Smictom</span>
                </h2>
                <p class="mb-3"> Le but de cette page est d’aider le responsable de collecte a organiser <br>
                    la planification de ses tournées ainsi que de lui proposer des tournées optimisées.<br>
                    Le fait d’optimiser les tournées de conteneurs permet de: <br>

                    -Diminuer les risques pour les riverains. (dû au conteneurs débordants)<br>
                    -Diminuer la consommation de carburant.<br>
                    -Gagner du temps dû à l’optimisation des tournées.
                </p>
                <div class="intro-button mx-auto">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-section cta">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <div class="cta-inner text-center rounded">
                    <h2 class="section-heading mb-4">
                        <span class="section-heading-upper">SITE Crée</span>
                        <span class="section-heading-lower">Par</span>
                    </h2>
                    <p class="mb-0">ANTHONE Benjamin<br>
                        VANLOOT Gabriel
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

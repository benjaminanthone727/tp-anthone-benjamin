<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_conteneur extends CI_Controller {

     public function __construct() {
        parent::__construct();
        $this->load->helper("url"); 
     }
	public function index()
	{
                $data['titre'] = 'Collecte de verre';
                $data['Tire_Princ'] = 'Un projet pilote de borne connectée'; 
                $page = $this->load->view('V_detailles_conteneurs', $data,true);
                $this->load->view('commun/V_template', array('contenu' => $page));
               
              
    }
      /*  public function search(){
            $search_name = $this->input->get('search_string');
            if ($search_name!=NULL) 
            {  
                
                $array_resultat = $this->M_conteneur->select_search_by_page();
                $data['result'] = $array_resultat;   
               $page = $this->load->view('conteneur/V_liste_conteneurs', $data,true);
               $this->load->view('commun/V_template', array('contenu' => $page));
              
             } 
            else {$this->index();
            }
        }
        
        public function detail()
        {
            $prmid= $this->uri->segment(3);
            $array_resultat = $this->M_conteneur->select_detail_by_conteneur($prmid);
            $data['result'] = $array_resultat; 
           $page = $this->load->view('conteneur/V_detail_conteneur', $data,true);
           $this->load->view('commun/V_template', array('contenu' => $page));
        }
        */
}


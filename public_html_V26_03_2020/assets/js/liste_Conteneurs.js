$(document).ready(function() {



    $.ajax({
        type: "GET",
        url: "http://localhost:82/CollecteVerre/index.php/REST/conteneur/index/", // Url du service web + le paramétre data-id  du bouton 
        dataType: "json",
        json: "json",
        success: onGetSuccess,
        error: onGetError
    });

    function onGetSuccess(reponse, status) {


        reponse.forEach(function(element) {

            var tauxremplissage; // On stock la mesure du conteneur 
            var emplacement; // Stock l'emplacement du conteneur 

            switch (element.VolumeMesureActuel != null) { //Si le taux de remplissage est différent de null

                case element.VolumeMesureActuel == 0: //si le taux de remplissage vaut 0
                    tauxremplissage = 0; //enregistre "0%" dans un variable local 
                    Emplacement = element.AddrEmplacement;
                    break;

                case element.VolumeMesureActuel == 1:
                    tauxremplissage = 25;
                    Emplacement = element.AddrEmplacement;
                    break;

                case element.VolumeMesureActuel == 2:
                    tauxremplissage = 50;
                    Emplacement = element.AddrEmplacement;
                    break;

                case element.VolumeMesureActuel == 3:
                    tauxremplissage = 75;
                    Emplacement = element.AddrEmplacement;
                    break;

                case element.VolumeMesureActuel == 4:
                    tauxremplissage = 100;
                    Emplacement = element.AddrEmplacement;
                    break;

                default:
                    tauxremplissage = 404;
                    alert("erreur bdd")
            }

            if (tauxremplissage >= 25) {
                $("#addresse").append(Emplacement);

            }
        });

    }

    function onGetError(reponse, status) {
        alert("erreur 404");
    }

});